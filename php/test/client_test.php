<?php

$apiKey = "API_KEY";
$apiSecret = "API_SECRET";

require_once(dirname(__FILE__).'/../quicklizard_api_client.php');

$qlClient = new QuicklizardApiClient();
$qlClient->configure($apiKey, $apiSecret);
$response = $qlClient->get('/api/v2/products');

print_r($response);

?>