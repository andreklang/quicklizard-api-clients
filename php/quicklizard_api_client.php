<?php

/**
 * Class QuicklizardApiClient
 *
 * The client libraries expose a simple API to perform GET and POST
 * requests against the Quicklizard API. Additionally a download
 * method is available to perform GET requests that download files.
 *
 * @link https://bitbucket.org/quicklizard/quicklizard-api-clients
 */
class QuicklizardApiClient {

    /** @var null|string */
    private $api_key = null;

    /** @var null|string */
    private $api_secret = null;

    /** @var string */
    private $host = 'https://api.quicklizard.com';

    /**
     * @param string $api_key
     * @param string $api_secret
     */
    public function configure($api_key, $api_secret){
        $this->api_key = $api_key;
        $this->api_secret = $api_secret;
    }

    /**
     * @param string $request_path
     * @return string|false
     */
    public function get($request_path) {
        $uri = parse_url($this->host.$request_path);
        $parts = $this->sign_request($uri['path'], isset($uri['query']) ? $uri['query'] : '' );
        $request_url = $this->host.$request_path.'?'.$parts['qs'];
        $headers = array('API_KEY: '.$this->api_key, 'API_DIGEST: '.$parts['digest']);
        $curl_options = array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $request_url,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_FOLLOWLOCATION => true
        );        
        $curl = curl_init();
        curl_setopt_array($curl, $curl_options);
        $result = curl_exec($curl);
        curl_close($curl);
        return $result;
    }

    /**
     * @param string $request_path
     * @param string $request_body
     * @return string|false
     */
    public function post($request_path, $request_body) {
        $uri = parse_url($this->host.$request_path);
        $parts = $this->sign_request($uri['path'], isset($uri['query']) ? $uri['query'] : '', $request_body);
        $request_url = $this->host.$request_path.'?'.$parts['qs'];
        $headers = array('API_KEY: '.$this->api_key, 'API_DIGEST: '.$parts['digest']);
        $curl_options = array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $request_url,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $parts['body']
        );
        $curl = curl_init();
        curl_setopt_array($curl, $curl_options);
        $result = curl_exec($curl);
        curl_close($curl);
        return $result;
    }

    /**
     * @param string $request_path
     * @param string $local_file path to local target file
     */
    public function download($request_path, $local_file) {        
        set_time_limit(0);
        $fp = fopen ($local_file, 'w+');
        $uri = parse_url($this->host.$request_path);
        $parts = $this->sign_request($uri['path'], isset($uri['query']) ? $uri['query'] : '');
        $request_url = $this->host.$request_path.'?'.$parts['qs'];
        $headers = array('API_KEY: '.$this->api_key, 'API_DIGEST: '.$parts['digest']);
        $curl_options = array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $request_url,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_FILE => $fp
        );        
        $curl = curl_init();
        curl_setopt_array($curl, $curl_options);
        curl_exec($curl);
        curl_close($curl);
    }

    /**
     * @param $path
     * @param null|string $qs query string
     * @param null|string $body
     * @return array
     */
    private function sign_request($path, $qs = null, $body = null) {
        $parts = array($path);
        $now = time() * 1000;
        if($qs !== null) {
            $qs .= '&qts='.$now;
            array_push($parts,$qs);
        } else if($body !== null){
            $body .= '&qts='.$now;
            array_push($parts,$body);
        } else {
            $qs = 'qts='.$now;
            array_push($parts,$qs);
        }
        array_push($parts, $this->api_secret);
        $digest = hash('sha256',implode('',$parts));
        return array(
            'path' => $path,
            'qs' => $qs,
            'body' => $body,
            'digest' => $digest,
        );
    }

}